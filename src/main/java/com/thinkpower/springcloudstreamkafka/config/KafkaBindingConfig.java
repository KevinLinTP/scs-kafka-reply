package com.thinkpower.springcloudstreamkafka.config;

import com.thinkpower.springcloudstreamkafka.utils.PartitionKeyExtractor;
import com.thinkpower.springcloudstreamkafka.utils.PartitionSelector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaBindingConfig {

    @Bean("PartitionKeyExtractor")
    public PartitionKeyExtractor partitionKeyExtractor() {
        return new PartitionKeyExtractor();
    }

    @Bean("PartitionSelector")
    public PartitionSelector partitionSelector() {
        return new PartitionSelector();
    }

}
